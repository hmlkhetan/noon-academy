package noonacademy.haimaldemo.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import noonacademy.haimaldemo.R;
import noonacademy.haimaldemo.activities.AddSubjects;
import noonacademy.haimaldemo.activities.DrawActivity;
import noonacademy.haimaldemo.activities.Subjects;
import noonacademy.haimaldemo.utils.LogForDebug;

/**
 * Created by haimal on 13/06/17.
 */
public class NavigationDrawerAdapter extends RecyclerView.Adapter<NavigationDrawerAdapter.ViewHolder> {

    private static final String TAG = NavigationDrawerAdapter.class.getSimpleName();

    private Context context;
    private String[] nav_item_titles;
    private int[] nav_item_icons;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private DrawerLayout drawerLayout;

    public NavigationDrawerAdapter(Context context, String[] nav_item_titles, int[] nav_item_icons, ActionBarDrawerToggle actionBarDrawerToggle, DrawerLayout drawerLayout) {
        this.context = context;
        this.nav_item_titles = nav_item_titles;
        this.nav_item_icons = nav_item_icons;
        this.actionBarDrawerToggle = actionBarDrawerToggle;
        this.drawerLayout = drawerLayout;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_nav_drawer_menu, parent, false);
        return new ViewHolder(view, viewType);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
            setupItem(holder, position);
        } catch (Exception e) {
            LogForDebug.displayLog(TAG, e.getMessage());
        }
    }

    @Override
    public int getItemCount() {
        return 3;
    }

    private void setupItem(final ViewHolder holder, int position) {

        holder.nav_icon.setImageDrawable(context.getResources().getDrawable(nav_item_icons[position]));
        holder.nav_title.setText(nav_item_titles[position]);
        holder.item_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigationDrawerItemClicked(holder.getAdapterPosition());
            }
        });
    }

    private void navigationDrawerItemClicked(int i) {

        switch (i) {

            case 0:
                Intent intent = new Intent(context, Subjects.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
                break;

            case 1:
                Intent intent2 = new Intent(context, AddSubjects.class);
                intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent2);
                break;

            case 2:

                Intent intent3 = new Intent(context, DrawActivity.class);
                intent3.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent3);
                break;

        }
        drawerLayout.closeDrawers();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        //For Navigation Drawer Item elements
        ImageView nav_icon;
        TextView nav_title;
        FrameLayout item_layout;


        ViewHolder(View itemView, int viewType) {
            super(itemView);

            item_layout = (FrameLayout) itemView.findViewById(R.id.nav_item_frame_layout);
            nav_icon = (ImageView) itemView.findViewById(R.id.nav_item_icon);
            nav_title = (TextView) itemView.findViewById(R.id.nav_title_text);
        }

    }
}
