package noonacademy.haimaldemo.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import noonacademy.haimaldemo.R;
import noonacademy.haimaldemo.activities.Subjects;
import noonacademy.haimaldemo.model.Subject;
import noonacademy.haimaldemo.utils.CodeDecode;

/**
 * Created by haimal on 13/06/17.
 */

public class SubjectsAdapter extends RecyclerView.Adapter<SubjectsAdapter.ViewHolder> {


    private Context context;
    private List<Subject> subjectList;

    public SubjectsAdapter(Context context, List<Subject> subjectList) {
        this.context = context;
        this.subjectList = subjectList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_single_subject, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        holder.tv_subject_name.setText(subjectList.get(position).getName());
        holder.tv_subject_description.setText(subjectList.get(position).getDescription());
        if (subjectList.get(position).getImage() == null || subjectList.get(position).getImage().equals("")) {
            holder.iv_subject_image.setImageResource(R.drawable.subjects);
        } else {
            holder.iv_subject_image.setImageBitmap(CodeDecode.decodeBase64(subjectList.get(position).getImage()));
        }

        holder.tv_delete_subject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                subjectList.get(holder.getAdapterPosition()).delete(context);
                subjectList.remove(holder.getAdapterPosition());
                notifyItemRemoved(holder.getAdapterPosition());
                if (subjectList.size() == 0) {
                    ((Subjects) context).updateUI();
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return subjectList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView iv_subject_image;
        TextView tv_subject_name, tv_subject_description, tv_delete_subject;

        ViewHolder(View itemView) {
            super(itemView);

            iv_subject_image = (ImageView) itemView.findViewById(R.id.iv_subject_pic);
            tv_subject_name = (TextView) itemView.findViewById(R.id.tv_subject_name);
            tv_subject_description = (TextView) itemView.findViewById(R.id.tv_subject_description);
            tv_delete_subject = (TextView) itemView.findViewById(R.id.tv_delete_subject);


        }
    }
}
