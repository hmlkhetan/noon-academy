package noonacademy.haimaldemo.utils;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * <h1>Class to maintain a consistent spacing around the items in a recycler view item</h1>
 *
 * @author Haimal Khetan
 * @version 1.0
 * @since 20/4/2016
 */

public class SpacesItemDecoration extends RecyclerView.ItemDecoration {

    /**
     * amount of space in dp to be given between two recycler view components.
     */

    private final int mSpace;


    /**
     * Constructor to set the offset spacing value.
     */

    public SpacesItemDecoration(int space) {
        this.mSpace = space;
    }


    /**
     * Method to set the offsets to all the items on the recycler view.
     */


    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.left = mSpace;
        outRect.right = mSpace;
        outRect.bottom = mSpace;

        // Add top margin only for the first item to avoid double space between items
        if (parent.getChildPosition(view) == 0)
            outRect.top = mSpace;
        else
            outRect.top = 0;
    }
}

