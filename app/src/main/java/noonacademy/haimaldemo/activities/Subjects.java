package noonacademy.haimaldemo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import noonacademy.haimaldemo.R;
import noonacademy.haimaldemo.adapters.SubjectsAdapter;
import noonacademy.haimaldemo.handlers.DatabaseHandler;
import noonacademy.haimaldemo.model.Subject;
import noonacademy.haimaldemo.utils.SpacesItemDecoration;

/**
 * Created by haimal on 13/06/17.
 */

public class Subjects extends AppCompatActivity implements View.OnClickListener {

    DatabaseHandler db;
    TextView tv_no_subjects;
    RecyclerView rv_show_subjects;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_show_subjects);
        initializeVars();

        if (db.getSubjectsCount() > 0) {
            setupUI();
        } else {
            tv_no_subjects.setVisibility(View.VISIBLE);
            tv_no_subjects.setOnClickListener(this);
        }
    }

    private void setupUI() {

        List<Subject> subjectList = db.getAllSubjects();
        rv_show_subjects.setVisibility(View.VISIBLE);
        rv_show_subjects.setLayoutManager(new LinearLayoutManager(this));
        rv_show_subjects.setHasFixedSize(true);
        rv_show_subjects.addItemDecoration(new SpacesItemDecoration(15));
        rv_show_subjects.setAdapter(new SubjectsAdapter(this, subjectList));

    }

    private void initializeVars() {

        getSupportActionBar().setTitle("Subjects");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);


        db = DatabaseHandler.getDB(this);


        tv_no_subjects = (TextView) findViewById(R.id.tv_no_subjects);
        tv_no_subjects.setOnClickListener(this);
        rv_show_subjects = (RecyclerView) findViewById(R.id.rv_show_subjects);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_no_subjects:
                gotoAddSubjects();
                break;
        }
    }

    public void updateUI() {
        tv_no_subjects.setVisibility(View.VISIBLE);
        rv_show_subjects.setVisibility(View.GONE);
    }

    private void gotoAddSubjects() {
        Intent intent = new Intent(this, AddSubjects.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            onBackPressed();
        return true;
    }
}
