package noonacademy.haimaldemo.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import noonacademy.haimaldemo.R;
import noonacademy.haimaldemo.adapters.NavigationDrawerAdapter;
import noonacademy.haimaldemo.utils.LogForDebug;
import noonacademy.haimaldemo.handlers.SharedPrefsHandler;

/**
 * Created by haimal on 13/06/17.
 */

public class HomeScreen extends AppCompatActivity {

    private final String TAG = HomeScreen.class.getSimpleName();
    DrawerLayout drawerLayout;
    CoordinatorLayout coordinatorLayout;
    RecyclerView nav_recycler_view;
    NavigationDrawerAdapter navigationDrawerAdapter;
    SharedPrefsHandler sharedPrefsHandler;
    ActionBarDrawerToggle actionBarDrawerToggle;
    String[] nav_item_titles;
    int[] nav_item_icons = {R.drawable.subjects,R.drawable.add,R.drawable.draw};


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_homescreen);
        initializeVars();
        setupNavigationDrawer();

    }


    /**
     * It initializes all required variables & views in the activity, sets up the navigation drawer with ActionBarDrawerToggle.
     */
    private void initializeVars() {
        try {

            drawerLayout = (DrawerLayout) findViewById(R.id.DrawerLayout);
            coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
            nav_recycler_view = (RecyclerView) findViewById(R.id.nav_recycler_view);
            sharedPrefsHandler = new SharedPrefsHandler(this);

            nav_item_titles = getResources().getStringArray(R.array.nav_items_title);

            actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.opendrawer, R.string.closedrawer);
            drawerLayout.addDrawerListener(actionBarDrawerToggle);
            actionBarDrawerToggle.syncState();

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);

        } catch (Exception e) {
            LogForDebug.displayLog(TAG, e.getMessage());
        }
    }


    private void setupNavigationDrawer() {
        nav_recycler_view.setLayoutManager(new LinearLayoutManager(this));
        nav_recycler_view.setHasFixedSize(true);
        navigationDrawerAdapter = new NavigationDrawerAdapter(this, nav_item_titles,nav_item_icons, actionBarDrawerToggle, drawerLayout);
        nav_recycler_view.setAdapter(navigationDrawerAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Passing the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        return actionBarDrawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }



}
