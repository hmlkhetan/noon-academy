package noonacademy.haimaldemo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;

import noonacademy.haimaldemo.R;
import noonacademy.haimaldemo.utils.LogForDebug;
import noonacademy.haimaldemo.handlers.SharedPrefsHandler;

/**
 * Created by haimal on 12/06/2017.
 */

public class Login extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {

    private static final String TAG = Login.class.getSimpleName();
    private static final int RC_SIGN_IN = 1308;
    /**
     * Sign-in to request the user's ID, email address, and basic profile.
     */
    GoogleSignInOptions gso;
    GoogleApiClient mGoogleApiClient;
    SignInButton google_sign_in_button;
    private SharedPrefsHandler sharedPrefsHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initiateVars();
        LogForDebug.displayLog(TAG, TAG);
        //goto HomeScreen directly if user already logged in, should be check usually on splash screen, but since demo app, checking here.
        if (sharedPrefsHandler.getUserLoggedIn().equals("true"))
            gotoHomeScreen(true);
        else
            setupGoogleSignIn();

    }

    private void setupGoogleSignIn() {
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

    }

    private void initiateVars() {

        sharedPrefsHandler = SharedPrefsHandler.getSharedPrefsHandler(this);
        // Set the dimensions of the sign-in button.
        google_sign_in_button = (SignInButton) findViewById(R.id.sign_in_button);
        google_sign_in_button.setSize(SignInButton.SIZE_STANDARD);
        google_sign_in_button.setOnClickListener(this);

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this, "Oops! Login Failed, Please check your internet connection and try again!", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.sign_in_button:
                signIn();
                break;
        }
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        LogForDebug.displayLog(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, save user data & goto Homescreen
            LogForDebug.displayLog(TAG, "Login Successful");
            GoogleSignInAccount acct = result.getSignInAccount();
            saveLoginDetailsToSharedPrefs(acct);
            gotoHomeScreen(true);
        } else {
            //SignIn Unsuccessful, show Toast
            Toast.makeText(this, "Oops! Login Failed, Please check your internet connection and try again!", Toast.LENGTH_LONG).show();
        }
    }

    private void gotoHomeScreen(boolean loginSuccess) {

        if (loginSuccess) {
            Intent intent = new Intent(this, HomeScreen.class);
            startActivity(intent);
            finish();
            if (mGoogleApiClient != null)
                if (mGoogleApiClient.isConnected()) mGoogleApiClient.disconnect();

        }

    }


    private void saveLoginDetailsToSharedPrefs(GoogleSignInAccount acct) {

        sharedPrefsHandler.setUserLoggedIn("true");
        sharedPrefsHandler.setUserName(acct.getDisplayName());
        sharedPrefsHandler.setUserEmail(acct.getEmail());
        sharedPrefsHandler.setUserProfilePicUrl(String.valueOf(acct.getPhotoUrl()));
    }
}