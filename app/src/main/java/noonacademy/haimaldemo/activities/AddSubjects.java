package noonacademy.haimaldemo.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.IOException;

import noonacademy.haimaldemo.R;
import noonacademy.haimaldemo.model.Subject;
import noonacademy.haimaldemo.utils.CodeDecode;
import noonacademy.haimaldemo.utils.LogForDebug;

/**
 * Created by haimal on 13/06/17.
 */

public class AddSubjects extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = AddSubjects.class.getSimpleName();
    private static final int PICK_IMAGE_REQUEST = 1;
    private static String base64Image = "";
    EditText et_subject_name, et_subject_description;
    ImageView iv_subject_image;
    Button btn_add_subject;
    Boolean sub_name_entered = false, sub_description_entered = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_add_subjects);
        initializeVars();
    }

    private void initializeVars() {

        getSupportActionBar().setTitle("Add Subjects");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        et_subject_name = (EditText) findViewById(R.id.et_subject_name);
        et_subject_description = (EditText) findViewById(R.id.et_subject_description);

        iv_subject_image = (ImageView) findViewById(R.id.iv_subject_image);
        iv_subject_image.setOnClickListener(this);

        btn_add_subject = (Button) findViewById(R.id.btn_add_subject);
        btn_add_subject.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.iv_subject_image:
                chooseSubjectImage();
                break;

            case R.id.btn_add_subject:
                validateEntries();
                break;
        }
    }

    private void chooseSubjectImage() {
        Intent intent = new Intent();
        // Show only images, no videos or anything else
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    private void validateEntries() {
        String missingParam = "";

        if (et_subject_name.getText().toString().length() > 0)
            sub_name_entered = true;
        else
            missingParam = "Subject Name";


        if (et_subject_description.getText().toString().length() > 0)
            sub_description_entered = true;
        else
            missingParam += missingParam.length() > 0 ? " & Subject Description" : "Subject Description";


        if (sub_name_entered && sub_description_entered) {
            resetMandatoryCounter();
            addSubjectToDatabase();
        } else {
            showMandatoryToast(missingParam);
        }

    }

    private void addSubjectToDatabase() {
        LogForDebug.displayLog(TAG, "adding subject to database");

        Subject subject = new Subject();
        subject.setName(et_subject_name.getText().toString());
        subject.setDescription(et_subject_description.getText().toString());
        subject.setImage(base64Image);
        if (subject.addToDatabase(this)) {
            clearAllFields();
        }


    }

    private void clearAllFields() {
        et_subject_name.getText().clear();
        et_subject_description.getText().clear();
        iv_subject_image.setImageResource(R.drawable.subjects);
        base64Image = "";
    }

    private void showMandatoryToast(String missingParam) {
        Toast.makeText(this, missingParam + " is required to proceed.", Toast.LENGTH_SHORT).show();
        resetMandatoryCounter();
    }

    private void resetMandatoryCounter() {
        sub_name_entered = false;
        sub_description_entered = false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri uri = data.getData();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                iv_subject_image.setImageBitmap(bitmap);

                convertImagetoBase64(bitmap);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void convertImagetoBase64(Bitmap bitmap) {
        base64Image = CodeDecode.encodeTobase64(bitmap);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            onBackPressed();
        return true;
    }
}
