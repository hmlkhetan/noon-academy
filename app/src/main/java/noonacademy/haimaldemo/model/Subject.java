package noonacademy.haimaldemo.model;

import android.content.Context;
import android.widget.Toast;

import noonacademy.haimaldemo.handlers.DatabaseHandler;

/**
 * Created by haimal on 13/06/17.
 */

public class Subject {

    private int id;
    private String name, description, image;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Boolean addToDatabase(Context context) {
        if (DatabaseHandler.getDB(context).addSubject(this)) {
            Toast.makeText(context, "Added Subject Successfully!", Toast.LENGTH_SHORT).show();
            return true;
        } else {
            Toast.makeText(context, "Oops! Something went wrong, please try again.", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    public void delete(Context context) {
        if (DatabaseHandler.getDB(context).deleteSubject(this)) {
            Toast.makeText(context, "Subject Deleted Successfully!", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, "Oops! Something went wrong, please try again.", Toast.LENGTH_SHORT).show();
        }
    }
}
