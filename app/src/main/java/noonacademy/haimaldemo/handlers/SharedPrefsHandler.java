package noonacademy.haimaldemo.handlers;

import android.content.Context;
import android.content.SharedPreferences;

import noonacademy.haimaldemo.utils.LogForDebug;


public class SharedPrefsHandler {

    private static final String TAG = SharedPrefsHandler.class.getSimpleName();
    private static final String SO_PREFERENCES_FILE = "NOON_PREFERENCES_FILE";
    private static final String USER_LOGGED_IN = "USER_LOGGED_IN";
    private static final String USER_NAME = "USER_NAME";
    private static final String USER_EMAIL = "USER_EMAIL";
    private static final String USER_PROFILE_PIC_URL = "USER_PROFILE_PIC_URL";

    private static SharedPrefsHandler sharedPrefsHandler;
    private static SharedPreferences sharedPreferences;

    public SharedPrefsHandler(Context context) {
        sharedPreferences = context.getSharedPreferences(SO_PREFERENCES_FILE, Context.MODE_PRIVATE);
    }

    public static SharedPrefsHandler getSharedPrefsHandler(Context context) {
        if (sharedPrefsHandler == null)
            sharedPrefsHandler = new SharedPrefsHandler(context);

        return sharedPrefsHandler;
    }

    private void writeToSharedPrefsFile(String key, String value) {
        try {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(key, value);
            editor.apply();
        } catch (Exception e) {
            LogForDebug.displayLog(TAG, e.getMessage());
        }
    }

    public String getUserLoggedIn() {
        return sharedPreferences.getString(USER_LOGGED_IN, "");
    }

    public void setUserLoggedIn(String userLoggedIn) {
        writeToSharedPrefsFile(USER_LOGGED_IN, userLoggedIn);
    }

    public String getUserName() {
        return sharedPreferences.getString(USER_NAME, "");
    }

    public void setUserName(String userName) {
        writeToSharedPrefsFile(USER_NAME, userName);
    }

    public String getUserEmail() {
        return sharedPreferences.getString(USER_EMAIL, "");
    }

    public void setUserEmail(String userEmail) {
        writeToSharedPrefsFile(USER_EMAIL, userEmail);
    }

    public String getUserProfilePicUrl() {
        return sharedPreferences.getString(USER_PROFILE_PIC_URL, "");
    }

    public void setUserProfilePicUrl(String userProfilePicUrl) {
        writeToSharedPrefsFile(USER_PROFILE_PIC_URL, userProfilePicUrl);
    }

    public void clear() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }
}
