package noonacademy.haimaldemo.handlers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import noonacademy.haimaldemo.model.Subject;
import noonacademy.haimaldemo.utils.LogForDebug;

/**
 * Created by haimal on 13/06/17.
 */

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final String TAG = DatabaseHandler.class.getSimpleName();
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "NOON DATABASE";
    /**
     * TABLE TO STORE USER'S ALL DETAILS
     */
    private static final String TABLE_SUBJECTS = "table_subjects";
    //Keys for TABLE_SUBJECTS
    private static final String KEY_ID = "id";
    private static final String KEY_SUBJECT_NAME = "name";
    private static final String KEY_SUBJECT_DESCRIPTION = "description";
    private static final String KEY_SUBJECT_B64IMAGE = "b64_image";
    private static DatabaseHandler db;

    private DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static DatabaseHandler getDB(Context context) {
        if (db == null)
            db = new DatabaseHandler(context);

        return db;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //CREATE TABLE SUBJECTS
        String CREATE_TABLE_SUBJECTS = "CREATE TABLE " + TABLE_SUBJECTS + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_SUBJECT_NAME + " TEXT,"
                + KEY_SUBJECT_DESCRIPTION + " TEXT,"
                + KEY_SUBJECT_B64IMAGE + " TEXT" + ")";
        db.execSQL(CREATE_TABLE_SUBJECTS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SUBJECTS);
        onCreate(db);
    }

    public int getSubjectsCount() {

        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_SUBJECTS;
        Cursor cursor = db.rawQuery(query, null);
        int rowCount = cursor.getCount();
        cursor.close();
        db.close();
        return rowCount;
    }

    //ADD NEW SUBJECT TO TABLE_SUBJECTS
    public Boolean addSubject(Subject subject) {

        LogForDebug.displayLog(TAG, subject.getName());

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_SUBJECT_NAME, subject.getName());
        values.put(KEY_SUBJECT_DESCRIPTION, subject.getDescription());
        values.put(KEY_SUBJECT_B64IMAGE, subject.getImage());
        db.insert(TABLE_SUBJECTS, null, values);
        db.close();

        return true;
    }

    //TO GET ALL THE SUBJECTS.
    public List<Subject> getAllSubjects() {
        try {

            List<Subject> subjectList = new ArrayList<>();
            String query = "SELECT * FROM " + TABLE_SUBJECTS;
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(query, null);

            cursor.moveToFirst();
            if (cursor.getCount() > 0) {
                do {
                    Subject subject = new Subject();
                    subject.setId(cursor.getInt(0));
                    subject.setName(cursor.getString(1));
                    subject.setDescription(cursor.getString(2));
                    subject.setImage(cursor.getString(3));

                    subjectList.add(subject);
                } while (cursor.moveToNext());
            }
            cursor.close();
            db.close();

            return subjectList;
        } catch (Exception e) {
            LogForDebug.displayLog(TAG, e.getMessage());
            return null;
        }
    }

    public boolean deleteSubject(Subject subject) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String query = "DELETE FROM " + TABLE_SUBJECTS + " WHERE " + KEY_ID + " =" + subject.getId();
            db.execSQL(query);
            return true;
        } catch (Exception e) {
            LogForDebug.displayLog(TAG, e.getMessage());
            return false;
        }
    }

}
